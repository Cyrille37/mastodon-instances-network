#!/usr/bin/env php
<?php
/**
 * Beaucoup d'erreur résolution DNS, ici on refait le tour des domaines dans la BdD,
 * pour afficher ceux qu'ils faut rescanner.
 */

declare(strict_types=1);
// tick use required for pcntl_signal
declare(ticks=1);

require_once(__DIR__.'/../vendor/autoload.php');

use Cyrille37\MastoInstNet\Common;
use Cyrille37\MastoInstNet\DAO;
use Cyrille37\MastoInstNet\Out;
use Cyrille37\MastoInstNet\Servers;
use Cyrille37\MastoInstNet\ServersStatus;

Common::$db_readonly = true;
Common::init();

$start_at = new \DateTimeImmutable('now', Common::getTimezone());

//DAO::transaction(function () {
/**
 * @var Servers $server
 */
foreach (Servers::all() as $server) {

    $status = ServersStatus::lastError($server);

    // https://www.php.net/manual/fr/function.dns-get-record
    $dns = dns_get_record($server->domain, DNS_A);

    if ($status == null) {
        if (empty($dns)) {
            //Out::println($server->domain, ': NO DNS');
        } else {
            //Out::println($server->domain, ': ', $res[0]['ip']);
        }
    } else {
        if (empty($dns)) {
            if (ServersStatus::isError($status)) {
            } else {
                //Out::println($server->domain, ': NO DNS but NO ERROR status: ', $status->status, ': ', $status->value);
            }
        } else {
            if (ServersStatus::isError($status)) {
                //Out::println($server->domain, ': DNS OK: ', $dns[0]['ip'], ', status: ', $status->status, ': ', $status->value);
                switch ($status->status) {
                    case 'error.curl':
                        switch ($status->value) {
                            case '6': // CURLE_COULDNT_RESOLVE_HOST
                                Out::println($server->domain, ': DNS OK but SCAN FAILED to resolve');
                                break;
                        }
                        break;
                }
            } else {
            }
        }
    }
}
//});

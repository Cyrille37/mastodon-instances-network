#!/usr/bin/env php
<?php

declare(strict_types=1);

error_reporting(-1);

require_once(__DIR__.'/../vendor/autoload.php');

use Cyrille37\MastoInstNet\Common;
use Cyrille37\MastoInstNet\Crawler\Crawler;
use Cyrille37\MastoInstNet\Env;
use Cyrille37\MastoInstNet\Out;
use Cyrille37\MastoInstNet\Parsers\Parser;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Response;
use GuzzleHttp\Promise;
use GuzzleHttp\Exception\ConnectException;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Exception\TransferException;
use GuzzleHttp\Pool;
use GuzzleHttp\Psr7\Request;

//Common::init();

/**
 * Crawlers:
 * - nodeinfo permits to discover ActivityPub server type (mastodon,peertube,..)
 * https://domain/.well-known/nodeinfo :
    "links": [
        {
            "href": "https://neenster.org/nodeinfo/2.0.json",
            "rel": "http://nodeinfo.diaspora.software/ns/schema/2.0"
        },
        {
            "href": "https://neenster.org/nodeinfo/2.1.json",
            "rel": "http://nodeinfo.diaspora.software/ns/schema/2.1"
        }
    ]
}

 * Mastodon:
 * - /api/v1/instance/peers
 * Peertube
 * - /api/v1/server/followers
 * - /api/v1/server/following

 */

$httpClient = new GuzzleHttp\Client([
    'base_uri' => 'https://code.devhost/dev/Mastodon/mastodon-instances-network/temp/target.php',
    'connect_timeout' => 1,
    'read_timeout' => 2,
    'timeout' => 2,
    'verify' => false,
    'User-Agent' => 'HttpALiveToMattermost-check',
]);

/**
 * @var array [domain=>[status,reason,data]]
 */
$results = [];
$batchsIndex = 0;
$domains = [
        'mamot.fr',
        'video.comptoir.net',
        'mastodon.social',
        'neenster.org',
];

$crawler = new Crawler();
$results = $crawler->crawl($httpClient, $domains);

Out::println( 'RESULT: ', var_export($results,true));
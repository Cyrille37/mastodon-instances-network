/**
 * DOM utility functions
 * 
 * Code from Joakim af Sandeberg https://github.com/jacomyal
 * https://github.com/jacomyal/sigma.js/tree/v1.2.1/examples
 */
var _ = {
    $: function (id) {
        return document.getElementById(id);
    },

    all: function (selectors) {
        return document.querySelectorAll(selectors);
    },

    removeClass: function (selectors, cssClass) {
        var nodes = document.querySelectorAll(selectors);
        var l = nodes.length;
        for (i = 0; i < l; i++) {
            var el = nodes[i];
            // Bootstrap compatibility
            el.className = el.className.replace(cssClass, '');
        }
    },

    addClass: function (selectors, cssClass) {
        var nodes = document.querySelectorAll(selectors);
        var l = nodes.length;
        for (i = 0; i < l; i++) {
            var el = nodes[i];
            // Bootstrap compatibility
            if (-1 == el.className.indexOf(cssClass)) {
                el.className += ' ' + cssClass;
            }
        }
    },

    show: function (selectors) {
        this.removeClass(selectors, 'hidden');
    },

    hide: function (selectors) {
        this.addClass(selectors, 'hidden');
    },

    toggle: function (selectors, cssClass) {
        var cssClass = cssClass || "hidden";
        var nodes = document.querySelectorAll(selectors);
        var l = nodes.length;
        for (i = 0; i < l; i++) {
            var el = nodes[i];
            //el.style.display = (el.style.display != 'none' ? 'none' : '' );
            // Bootstrap compatibility
            if (-1 !== el.className.indexOf(cssClass)) {
                el.className = el.className.replace(cssClass, '');
            } else {
                el.className += ' ' + cssClass;
            }
        }
    }
};

#!/usr/bin/env php
<?php
/**
 * graph-peerings.php
 * 
 * options:
 * -a or --alive : only output alive servers.
 * 
 */

declare(strict_types=1);

require_once('vendor/autoload.php');

use Cyrille37\MastoInstNet\Common;
use Cyrille37\MastoInstNet\Env;
use Cyrille37\MastoInstNet\Out;
use Cyrille37\MastoInstNet\Servers;
use Cyrille37\MastoInstNet\ServersPeers;
use Cyrille37\MastoInstNet\ServersStatus;
use Cyrille37\MastoInstNet\Stats;
use GuzzleHttp\Exception\ClientException;
use pcrov\JsonReader\JsonReader;

Common::$db_readonly = true;
Common::init();

$short_options = 'a::';
$long_options = ['alive::'];
$options = getopt($short_options, $long_options);

$aliveOnly = false;
if (isset($options['a']) || isset($options['alive'])) {
    $aliveOnly = true;
}

Out::println('Generating graph file from "', Env::get('DATABASE_DSN'), '"');
Out::println('aliveOnly: ', $aliveOnly ? 'On' : 'Off');

$graph = ['nodes' => [], 'edges' => []];

//
// Nodes
//

$sql = '
select S.id, S.domain, S.type, SS.status, ss.created_at as status_at
 from servers S
 join servers_status SS on SS.server_id=S.id
 where SS.created_at = (select max(created_at) from servers_status where server_id=S.id)
';
if ($aliveOnly)
    $sql .= ' and SS.status = "OK"';

// @var array $nodesIndex to filter edges for filtered nodes (not presents).
$nodesIndex = [];
$nodesCount = 0;
foreach (Servers::select($sql) as $server) {
    $nodesCount++;
    $nodesIndex[$server->id] = 1;
    $alive = $server->status == ServersStatus::STATUS_OK ? true : false;
    $graph['nodes'][] = [
        'id' => $server->id, 'label' => $server->domain, 'type' => $server->type,
        'alive' => $alive, 'status_at' => $server->status_at,
        'size' => 1, 'x' => rand(0, 1000), 'y' => rand(0, 1000),
    ];
}

//
// Edges
//

$sql = 'select * from servers_peers SP group by server_from, server_to';

$edgesCount = 0;
foreach (ServersPeers::select($sql) as $peering) {

    if ((!isset($nodesIndex[$peering->server_from])) || (!isset($nodesIndex[$peering->server_to]))) {
        Stats::inc('edges.skipped');
        continue;
    }
    $graph['edges'][] = [
        'source' => $peering->server_from, 'target' => $peering->server_to, 'id' => ++$edgesCount,
        //'type' => 'curvedArrow',
    ];
}

$filename = __DIR__ . '/public/data/peerings.json';
file_put_contents($filename, json_encode($graph));

Out::println('Stats: ', \var_export(Stats::stats(), true));
Out::println('Done with ', number_format($nodesCount), ' nodes and ', number_format($edgesCount), ' edges.');
Out::println('Generated file: ', $filename);

# Description

Explore les instances Mastodon (peers) à partir d'un serveur de départ
et stocke en base de données:
- Servers: les noms de domaine des instances ;
- ServersStatus: le serveur était-il joignable à la date "created_at" ;
- ServersPeers: le serveur "server_to" a été découvert sur le "server_from".

Pour la configuration voir `.env-example`.

## scan.php

Fait le scan des serveurs `peers` à une profondeur `SCAN_DEPTH`, par défaut 10.

`./scan.php --depth=5 pouet.april.org`

```
# Laptop + box@home (poor dns) + Db sqlite3 : servers/second=5.13
./scan.php --depth=2 amicale.net 
  'Crawler.error' => 30056,
  'Crawler.requests' => 34687,
  'Crawler.success' => 4631,
  'Links.opti' => 221,
  'Scan.depth' => 3,
  'Scan.servers' => 31615,
  'Servers.peers' => 2667357,
  'dao.insert' => 2734822,
  'dao.select' => 2729849,
  'dao.update' => 2946,
  'ellapsed.seconds' => 6162.657494068146,
  'start_at' => 1668263471.141697,
```

```
  'Config.Crawler concurrency: ' => '120',
  'Config.Db' => 'sqlite:mastodon-instances-network.sqlite',
  'Config.Depth max: ' => 2,
  'Config.Scan root domain: ' => 'amicale.net',
  'Config.Start at' => '2022-11-12 16:36:41',
  'Crawler.error' => 6178,
  'Crawler.requests' => 10660,
  'Crawler.success' => 4482,
  'Scan.depth' => 2,
  'Scan.servers' => 8093,
  'Servers.peers.subscribers' => 8092,
  'dao.insert' => 31182,
  'dao.select' => 38737,
  'dao.update' => 2200,
  'ellapsed.seconds' => 584.8845670223236,
  'servers/second' => 6397.982447275008,
  'start_at' => 1668271001.136486,
```


### options

- `--depth` la profondeur du scan. Les peers des peers jusqu'à `--depth`

## graph-peerings.php

Génère le fichier `public/data/peerings.json` pour afficher le graph avec `public/peerings-sigma.html` et `public/peerings-d3.html`.

`./graph-peerings.php`

### options

- `-a` or `--alive` : only output alive servers.

## peerings-sigma.html

Visualisation facile à réalisée grâce au fabuleux [Sigma.js](https://www.sigmajs.org/).

Testé avec 13 000 noeuds et 141 300 edges.

# Todo

## Fediverse ActivityPub

ActivityPub: From Decentralized to Distributed Social Networks

ActivityPub is all about actors, activities, and objects, but **doesn’t say anything about the instance itself** : the site that’s hosting all this. Various approaches are filling this gap: NodeInfo, Federation.md. 

- *dead* ~~http://activitypub.rocks/~~
- W3C
  - [Activity Vocabulary](https://www.w3.org/TR/activitystreams-vocabulary/#dfn-view)

- Tutos & discussions
  - [Guide for new ActivityPub implementers](https://socialhub.activitypub.rocks/t/guide-for-new-activitypub-implementers/479)
  - [Peertube discussion](https://github.com/Chocobozzz/PeerTube/issues/104#issuecomment-347476230)
  - [Fetching ActivityPub Feeds](https://www.gkbrk.com/2018/06/fetching-activitypub-feeds/)

EndPoints:
- https://video.comptoir.net/.well-known/webfinger

## Peertube

Les peertube sont traités comme "not alive".
@todo: trouver un traitement adapté.

- https://docs.joinpeertube.org/api-rest-reference.html#tag/Instance-Follows/paths/~1api~1v1~1server~1following/get
  - https://video.comptoir.net/api/v1/server/followers
  - https://video.comptoir.net/api/v1/server/following

# Mastodon doc

## Instance

- https://docs.joinmastodon.org/methods/instance/
  - /api/v1/instance Information about the server.
  - /api/v1/instance/peers Domains that this instance is aware of.
  - /api/v1/instance/activity Instance activity over the last 3 months, binned weekly.
- https://docs.joinmastodon.org/methods/instance/trends/
  - /api/v1/trends Tags that are being used more frequently within the past week.
- https://docs.joinmastodon.org/methods/instance/directory/
  - /api/v1/directory List accounts visible in the directory.
- https://docs.joinmastodon.org/methods/instance/custom_emojis/
  - /api/v1/custom_emojis Returns custom emojis that are available on the server.
- https://docs.joinmastodon.org/methods/admin/
  - User token + admin xxx
- https://docs.joinmastodon.org/methods/announcements/
  - /api/v1/announcements See all currently active announcements set by admins.
  - /api/v1/announcements/:id/dismiss Allows a user to mark the announcement as read.
  - /api/v1/announcements/:id/reactions/:name React to an announcement with an emoji.
  - /api/v1/announcements/:id/reactions/:name Undo a react emoji to an announcement.
- https://docs.joinmastodon.org/methods/proofs/
  - /api/proofs View identity proof (custom response defined by provider)
- https://docs.joinmastodon.org/methods/oembed/
  - For generating OEmbed previews.

# Packages

## SQlite

Problem d'accès concurent, même avec `$dbOpts[PDO::SQLITE_ATTR_OPEN_FLAGS] = PDO::SQLITE_OPEN_READONLY;`.

Peut-être en préparant les statements avant ...
- PDOException: SQLSTATE[HY000]: General error: 5 database is locked
  - https://www.php.net/manual/en/pdo.begintransaction.php#90239

## Guzzle

- [Request Options](https://docs.guzzlephp.org/en/stable/request-options.html)
- [How can I add custom cURL options?](https://docs.guzzlephp.org/en/stable/faq.html#how-can-i-add-custom-curl-options)
- [options for a curl multi handle](https://curl.se/libcurl/c/curl_multi_setopt.html)
- [libcurl errors](https://curl.se/libcurl/c/libcurl-errors.html)

## JsonReader

Pour lire des gros JSON en consommant moins de mémoire: 
https://github.com/pcrov/JsonReader

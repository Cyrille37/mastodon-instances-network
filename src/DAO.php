<?php

declare(strict_types=1);

namespace Cyrille37\MastoInstNet;

use Cyrille37\MastoInstNet\Stats;
use RuntimeException;

abstract class DAO
{
    const DATE_FORMAT = 'Y-m-d H:i:s';
    public static $db = null;
    public static $table = null;
    //public static $pk = null;
    public static $created_at = false;
    public static $updated_at = false;

    static public function all()
    {
        return self::find([]);
    }

    static public function findOne(array $args)
    {
        return self::find($args, true);
    }

    static public function select($sql, array $args = [], $findOne = false)
    {
        $sth = self::$db->prepare($sql);
        $sth->execute($args);

        Stats::inc('dao.select');

        if ($findOne)
            return $sth->fetch(\PDO::FETCH_OBJ);
        return $sth->fetchAll(\PDO::FETCH_OBJ);
    }

    static public function find(array $args = [], $findOne = false)
    {
        $sql = '';
        foreach ($args as $k => $v) {
            if ($sql != '')
                $sql .= ' AND ';
            $sql .= ' ' . $k . ' = :' . $k;
        }
        if (!empty($sql))
            $sql = 'select * from ' . static::$table . ' where ' . $sql;
        else
            $sql = 'select * from ' . static::$table;
        $sth = self::$db->prepare($sql);
        $sth->execute($args);

        Stats::inc('dao.select');

        if ($findOne)
            return $sth->fetch(\PDO::FETCH_OBJ);
        return $sth->fetchAll(\PDO::FETCH_OBJ);
    }

    static public function findOneOrCreate(array $args, array $createArgs = [], $updateAt = false)
    {
        $now_at = (new \DateTimeImmutable('now', Common::getTimezone()))->format(self::DATE_FORMAT);

        $obj = self::findOne($args);
        if ($obj) {
            if ($updateAt && static::$updated_at) {
                static::update($args, ['updated_at' => $now_at]);
                $obj->updated_at = $now_at;
            }
            return $obj;
        }

        if (static::$created_at) {
            if (!isset($createArgs['created_at']))
                $createArgs['created_at'] = $now_at;
        }
        if (static::$updated_at) {
            if (!isset($createArgs['updated_at']))
                $createArgs['updated_at'] = $now_at;
        }

        $createArgs = array_merge($args, $createArgs);
        $fields = [];
        $values = [];
        foreach ($createArgs as $k => $v) {
            $fields[] = $k;
            $values[] = ':' . $k;
        }

        $sql = 'insert into ' . static::$table . ' (' . implode(',', $fields) . ') values (' . implode(',', $values) . ')';
        $sth = self::$db->prepare($sql);
        $sth->execute($createArgs);

        Stats::inc('dao.insert');

        return self::findOne($args);
    }

    static public function insert(array $createArgs, $return = true)
    {
        $now_at = (new \DateTimeImmutable('now', Common::getTimezone()))->format(self::DATE_FORMAT);
        if (static::$created_at) {
            if (!isset($createArgs['created_at']))
                $createArgs['created_at'] = $now_at;
        }
        if (static::$updated_at) {
            if (!isset($createArgs['updated_at']))
                $createArgs['updated_at'] = $now_at;
        }

        $fields = [];
        $values = [];
        foreach ($createArgs as $k => $v) {
            $fields[] = $k;
            $values[] = ':' . $k;
        }

        $sql = 'insert into ' . static::$table . ' (' . implode(',', $fields) . ') values (' . implode(',', $values) . ')';
        $sth = self::$db->prepare($sql);
        $sth->execute($createArgs);

        Stats::inc('dao.insert');

        if ($return)
            return self::findOne($createArgs);
    }

    static public function insertMany(array $commonArgs, array $createArgs)
    {
        $now_at = (new \DateTimeImmutable('now', Common::getTimezone()))->format(self::DATE_FORMAT);
        if (static::$created_at) {
            if (!isset($commonArgs['created_at']))
                $commonArgs['created_at'] = $now_at;
        }
        if (static::$updated_at) {
            if (!isset($commonArgs['updated_at']))
                $commonArgs['updated_at'] = $now_at;
        }

        $fields = array_merge(array_keys($commonArgs), array_keys($createArgs[0]));
        $values = [];
        foreach ($createArgs as $row) {
            $values[] = array_merge(array_values($commonArgs), array_values($row));
        }

        $sql = 'insert into ' . static::$table . ' (' . implode(',', $fields) . ') values ';
        $positionals = array_fill(0, count($fields), '?');
        for ($i = 0; $i < count($values); $i++) {
            if ($i > 0)
                $sql .= ',';
            $sql .= '(' . implode(',', $positionals) . ')';
        }

        $sth = self::$db->prepare($sql);

        $positionals = [];
        foreach ($values as $value) {
            foreach ($value as $v) {
                $positionals[] = $v;
            }
        }

        $sth->execute($positionals);
        Stats::inc('dao.insert', count($createArgs));
    }

    static public function update(array $args, array $updateArgs, $insertIfNotExists = false )
    {
        if (static::$updated_at) {
            if (!isset($updateArgs['updated_at'])) {
                $now_at = (new \DateTimeImmutable('now', Common::getTimezone()))->format(self::DATE_FORMAT);
                $updateArgs['updated_at'] = $now_at;
            }
        }

        $sqlSet = [];
        foreach ($updateArgs as $k => $v) {
            $sqlSet[] = $k . '=:' . $k;
        }

        $sqlWhere = [];
        foreach ($args as $k => $v) {
            $sqlWhere[] = $k . '=:' . $k;
        }
        $sql = 'update ' . static::$table
            . ' set ' . implode(',', $sqlSet)
            . ' where ' . implode(' AND ', $sqlWhere);
        $sth = self::$db->prepare($sql);
        $args = \array_merge($args, $updateArgs);

        Stats::inc('dao.update');

        $res = $sth->execute($args);
        if( $insertIfNotExists && $res && $sth->rowCount() == 0 )
        {
            static::insert( array_merge($args,$updateArgs), false );
        }
    }

    public static function transaction(callable $callback)
    {
        self::$db->beginTransaction();
        try {
            $callback();
            self::$db->commit();
        } catch (\Exception $ex) {
            self::$db->rollback();
            throw $ex;
        }
    }
}

<?php

declare(strict_types=1);

namespace Cyrille37\MastoInstNet;

/**
 *
 * @property integer $server_from
 * @property integer $server_to
 * @property string $created_at
 */
class ServersPeers extends DAO
{

    public static $table = 'servers_peers';
    public static $created_at = true;
    public static $updated_at = true;
}

<?php

declare(strict_types=1);

declare(ticks=1);

namespace Cyrille37\MastoInstNet;

use Cyrille37\MastoInstNet\Crawler\Crawler;
use Cyrille37\MastoInstNet\Crawler\Result;

class Scanner
{

    public static function scan_server($domain, $depth_max)
    {
        static $scanned = ['domains' => [], 'links' => []];

        Stats::set('Config.Db', Common::getDbDsn());
        Stats::set('Config.Depth max: ', $depth_max);
        Stats::set('Config.Crawler concurrency: ', Crawler::getConcurrency());
        Stats::set('Scan.RootDomain: ', $domain);

        Stats::set('Scan.depth',0);
        Stats::inc('Scan.servers', 0);

        $servers = [];
        $servers[$domain] = Servers::findOneOrCreate(['domain' => $domain], []);

        /**
         * Already scanned domains & links to avoid too many Db accesses.
         */
        $scan_depth = 0;
    
        while (count($servers) > 0) {
    
            Out::debug('Already scanned: domains:', count($scanned['domains']), ', links:', count($scanned['links']));
    
            $domains = [];
            foreach ($servers as $domain => $server)
                $domains[] = $server->domain;

            Stats::inc('Scan.depth');
    
            $crawler = new Crawler();
            $results = $crawler->crawl(Common::getHttpClient(), $domains);

            Stats::inc('Scan.servers', count($servers));
    
            Out::debug('Updating ', count($results), ' servers status...');
    
            /**
             * @var Result $result
             */
            DAO::transaction(function () use (&$results, &$servers, &$scanned) {
                foreach ($results as $domain => $result) {
                    try {
                        $server = $servers[$domain];
                        $scanned['domains'][$domain] = $server;
                        self::serverStatusUpdate($server, $result);
                    } catch (\Exception $ex) {
                        Out::error($domain, ':', $ex->getMessage(), '. Stack: ', $ex->getTraceAsString());
                    }
                }
            });
    
            // Display Stats
            if( Stats::get('ellapsed.seconds') > 0 )
                Stats::set('servers/second', (1 * Stats::get('Scan.servers') / (1.0 * Stats::get('ellapsed.seconds'))) );
            Out::println(\var_export(Stats::stats(), true));
    
            Out::debug('Updating links...');
    
            $serversNext = [];
            foreach ($results as $domain => $result) {
                $server = $servers[$domain];
                if (!empty($result->subscribers)) {
                    Out::debug('Init subscribers:', count($result->subscribers));
                    DAO::transaction(function () use ($result, $server, &$scanned, &$serversNext) {
                        foreach ($result->subscribers as $domain) {
                            Stats::inc('Servers.peers.subscribers');
                            if (!isset($scanned['domains'][$domain])) {
                                $peer = Servers::findOneOrCreate(['domain' => $domain], []);
                                $serversNext[$peer->domain] = $peer;
                            } else {
                                $peer = $scanned['domains'][$domain];
                                Stats::inc('optimize.domains.skipped');
                            }
                            $link = $server->id . '-' . $peer->id;
                            if (!isset($scanned['links'][$link])) {
                                //ServersPeers::findOneOrCreate(['server_from' => $server->id, 'server_to' => $peer->id], [], true);
                                ServersPeers::update(['server_from' => $server->id, 'server_to' => $peer->id], [], true);
                                $scanned['links'][$link] = true;
                            } else
                                Stats::inc('optimize.links.skipped');
                        }
                    });
                }
                if (!empty($result->subscriptions)) {
                    Out::debug('Init subscriptions:', count($result->subscriptions));
                    DAO::transaction(function () use ($result, $server, &$scanned, &$serversNext) {
                        foreach ($result->subscriptions as $domain) {
                            Stats::inc('Servers.peers.subscriptions');
                            if (!isset($scanned['domains'][$domain])) {
                                $peer = Servers::findOneOrCreate(['domain' => $domain], []);
                                $serversNext[$peer->domain] = $peer;
                            } else {
                                $peer = $scanned['domains'][$domain];
                                Stats::inc('optimize.domains.skipped');
                            }
                            $link = $peer->id . '-' . $server->id;
                            if (!isset($scanned['links'][$link])) {
                                //ServersPeers::findOneOrCreate(['server_from' => $peer->id, 'server_to' => $server->id], [], true);
                                ServersPeers::update(['server_from' => $peer->id, 'server_to' => $server->id], [], true);
                                $scanned['links'][$link] = true;
                            } else
                                Stats::inc('optimize.links.skipped');
                        }
                    });
                }
            }
    
            if (++$scan_depth >= $depth_max) {
                Out::println('Finish, Scan depth max');
                break;
            }
    
            $servers = $serversNext;
        }
    }
    
    static protected function serverStatusUpdate($server, Result $result)
    {
        // Server type (software_name)
        if ($result->software_name && !$server->type) {
            $server->type = $result->software_name;
            Servers::update(['domain' => $server->domain], ['type' => $server->type]);
        } else if ($result->software_name && ($server->type != $result->software_name)) {
            ServersStatus::setStatus($server, 'software-change', $server->type);
            $server->type = $result->software_name;
            Servers::update(['domain' => $server->domain], ['type' => $server->type]);
        }
        // Error
        if ($result->error_code)
            ServersStatus::setStatus($server, $result->error_code, $result->error_reason);
        else {
            $statuses = [
                'http' => 'ok'
            ];
            if( $result->openRegistrations != null )
                $statuses['openRegistrations'] = $result->openRegistrations ;
            if( $result->localPosts != null )
                $statuses['localPosts'] = $result->localPosts ;
            if( $result->users_total != null )
                $statuses['users_total'] = $result->users_total ;
            ServersStatus::setStatusMany($server, $statuses);
        }
    }
    
}

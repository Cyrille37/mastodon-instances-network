<?php

declare(strict_types=1);

namespace Cyrille37\MastoInstNet;

use PDO;
use Psr\Http\Client\ClientInterface ;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Handler\CurlHandler;
use Cyrille37\MastoInstNet\DAO;

error_reporting(-1);

/**
 * 
 */
class Common
{
    /**
     * @var \GuzzleHttp\Client
     */
    static protected $http;
    static protected $timezone;
    static public $db_readonly = false;

    static function init()
    {
        // Start Stats as soon as possible
        Stats::set('start_at', \microtime(true));

        define('DEBUG', Env::get('DEBUG'));
        define('CACHE_FOLDER', Env::get('CACHE_FOLDER'));

        self::$timezone = new \DateTimeZone(Env::get('TIMEZONE', 'Etc/UTC'));

        // Database

        $dbOpts = [
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
        ];
        if (static::$db_readonly)
            $dbOpts[PDO::SQLITE_ATTR_OPEN_FLAGS] = PDO::SQLITE_OPEN_READONLY;

        $db = new \PDO(self::getDbDsn(), Env::get('DATABASE_USER'), Env::get('DATABASE_PASSWORD'), $dbOpts);
        // Should optimize Sqlite engine use
        // https://phiresky.github.io/blog/2020/sqlite-performance-tuning/
        // https://www.sqlite.org/pragma.html#pragma_synchronous
        // https://www.sqlite.org/wal.html
        $db->exec('PRAGMA journal_mode = WAL');
        $db->exec('PRAGMA synchronous = normal');
        $db->exec('PRAGMA temp_store = memory');
        DAO::$db = $db;

        // Http client

        /**
         * Guzzle Request Options
         * https://docs.guzzlephp.org/en/stable/request-options.html
         * Handlers
         * https://docs.guzzlephp.org/en/stable/handlers-and-middleware.html#handlers
         */
        //$handler = new CurlHandler();
        //$stack = HandlerStack::create($handler);
        self::$http = new \GuzzleHttp\Client(
            [
                //'handler' => $stack,
                'cookies' => false,
                'verify' => false,
                'handle_factory' => new \GuzzleHttp\Handler\CurlFactory(static::getCrawlerConcurrency()),
                // connect_timeout is currently only supported by the built-in cURL handler.
                'connect_timeout' => 4,
                'read_timeout' => 3,
                'timeout' => 5,
                // select_timeout: Optional timeout (in seconds) to block before timing out
                // while selecting curl handles. Defaults to 1 second.
                'select_timeout' => 2,
                'synchronous' => false ,
                'stream' => false,
                'options' => [
                    // curl_multi_setopt
                    // https://www.php.net/manual/fr/function.curl-multi-setopt.php
                    // max simultaneously open connections
                    CURLMOPT_MAX_TOTAL_CONNECTIONS => 1000,
                    CURLMOPT_MAXCONNECTS => 1000,
                    CURLMOPT_MAX_HOST_CONNECTIONS => 2,
                    // curl-setopt
                    // https://www.php.net/manual/fr/function.curl-setopt.php
                    CURLOPT_MAXCONNECTS => 1000,
                    CURLOPT_DNS_CACHE_TIMEOUT => 5 , // default: 120
                    CURLOPT_TCP_FASTOPEN => true,
                    CURLOPT_NOSIGNAL => false,
                    CURLOPT_TCP_KEEPALIVE => false,
                ],            
                'headers' => [
                    //'User-Agent' => 'Mozilla/5.0 (X11; Linux x86_64; rv:103.0) Gecko/20100101 Firefox/103.0',
                    'User-Agent' => 'cyrille37/mastodon-instances-network v1.0',
                    //'DNT' => '1',
                    //'Referer' => 'https://www.xxx/yyy',
                    //'Accept' => 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8',
                    'Accept' => 'application/json',
                    // Remove `br`, it seems badly managed ... issue#1
                    //'Accept-Encoding' => 'gzip, deflate, br',
                    'Accept-Encoding' => 'gzip, deflate',
                    //'Accept-Language' => 'en;q=0.7,pl;q=0.5,pt;q=0.3,pt-BR;q=0.2',
                    'Accept-Language' => 'en',
                    //'Upgrade-Insecure-Requests' => '1',
                    'Connection' => 'close',
                ],
            ]
        );
    }

    protected static $dbDsn ;

    public static function setDbDsn( $dsn )
    {
        if( self::$dbDsn )
            throw new \RuntimeException('DB DSN already set');
        self::$dbDsn = $dsn ;
    }
    public static function getDbDsn()
    {
        if( ! self::$dbDsn )
            self::$dbDsn = Env::get('DATABASE_DSN');
        return self::$dbDsn ;
    }

    public static function getTimezone()
    {
        return self::$timezone;
    }

    const DEFAULT_CONCURRENCY = 8;

    public static function getCrawlerConcurrency()
    {
        return Env::get('CRAWLER_CONCURRENCY', self::DEFAULT_CONCURRENCY);
    }

    public static function slug($s)
    {
        return preg_replace('~[^\pL\d]+~u', '-', $s);
    }

    public static function getHttpClient() : ClientInterface
    {
        return self::$http ;
    }

    public static function http_get($url, array $headers = [])
    {
        $tries = intval(Env::get('HTTP_TRIES', 3));
        $sleep = intval(Env::get('HTTP_SLEEP', 2));
        while ($tries) {
            Stats::inc('http');
            try {
                $res = self::$http->get($url, $headers);
                //echo ('http status: ' . $res->getStatusCode() . ', http content-type: ' . var_export($res->getHeader('content-type'), true));
                if ($res->getStatusCode() == 200)
                    return $res;
                Out::println('HTTP try ', $tries, ' status:', $res->getStatusCode());
            } catch (\GuzzleHttp\Exception\ServerException $ex) {
                Out::println('HTTP try ', $tries, ' ex:', $ex->getMessage());
            }
            $tries--;
            if ($sleep > 0)
                sleep($sleep);
        }
        throw new \Exception('HTTP Failed after ' . $tries . ' try⋅ies to get "' . $url . '"');
    }

    public static function isInternetAvailable(): bool
    {
        try {
            $res = @file_get_contents('https://google.com');
            if ($res)
                return true;
        } catch (\Exception $ex) {
        }
        return false;
    }
}

/**
 * 
 */
class Stats
{
    static protected $instance = null;
    public static function getInstance()
    {
        if (!self::$instance)
            self::$instance = new self();
        return self::$instance;
    }

    protected $stats = [];
    protected $startTime;
    protected function __construct()
    {
        $this->startTime = \microtime(true);
    }

    public static function inc($key, $inc = 1): void
    {
        static $stats;
        if (!$stats)
            $stats = self::getInstance();
        if (!isset($stats->stats[$key]))
            $stats->stats[$key] = $inc;
        else
            $stats->stats[$key] += $inc;
    }

    public static function get($key)
    {
        $stats = self::getInstance();
        if (!isset($stats->stats[$key]))
            return null;
        return $stats->stats[$key];
    }

    public static function set($key, $value)
    {
        $stats = self::getInstance();
        $stats->stats[$key] = $value;
    }

    public static function stats(): array
    {
        $stats = self::getInstance();
        $stats->stats['ellapsed.seconds'] = \microtime(true) - $stats->startTime;
        ksort($stats->stats);
        return $stats->stats;
    }
}

<?php
declare(strict_types=1);

namespace Cyrille37\MastoInstNet ;

/**
 * @property integer $id
 * @property string $domain
 * @property string $type
 * @property string $created_at
 * @property string $updated_at
 */
class Servers extends DAO {

	const TYPE_MASTODON = 'md';
	const TYPE_PEERTUBE = 'pt' ;

    public static $table = 'servers' ;
	public static $created_at = true ;

}

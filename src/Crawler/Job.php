<?php

declare(strict_types=1);

namespace Cyrille37\MastoInstNet\Crawler;

class Job {
    /**
     * @var string
     */
    public $domain ;
    /**
     * @var Parser
     */
    public $parser ;
    /**
     * temporary iteration Parser's data like pagination
     * @var Array
     */
    public $parserData ;

    public function __construct($domain)
    {
        $this->domain = $domain;
    }
}
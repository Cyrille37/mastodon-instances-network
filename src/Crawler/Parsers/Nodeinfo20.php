<?php

declare(strict_types=1);

namespace Cyrille37\MastoInstNet\Crawler\Parsers;

use Cyrille37\MastoInstNet\Crawler\Job;
use Cyrille37\MastoInstNet\Crawler\Result;
use Cyrille37\MastoInstNet\Out;

class Nodeinfo20 extends Parser
{
    const URI = '/nodeinfo/2.0.json';

    public static function parse($content, Job $job, Result $result): void
    {
        //Out::println(__METHOD__,' content: ', $content);

        $data = json_decode($content);
        if (!$data)
            throw new ParserException('Invalid JSON');

        if( isset($data->openRegistrations))
            $result->openRegistrations = $data->openRegistrations;

        if (isset($data->software)) {
            $result->software_name = $data->software->name;
            if( isset($data->software->version) )
                $result->software_version = $data->software->version;

            if (isset($data->usage)) {
                $result->localPosts = isset($data->usage->localPosts) ? $data->usage->localPosts : null;
                $result->localComments = isset($data->usage->localComments) ? $data->usage->localComments : null;
                $result->users_total = isset($data->usage->users->total) ? $data->usage->users->total : null;
                $result->users_activeMonth = isset($data->usage->users->activeMonth) ? $data->usage->users->activeMonth : null;
                $result->users_activeHalfyear = isset($data->usage->users->activeHalfyear) ? $data->usage->users->activeHalfyear : null;
            } else {
                //Out::debug(__METHOD__, ' domain:', $job->domain, ', Incomplete usage: ', \var_export($data, true));
            }

        } else {
            //Out::debug(__METHOD__, ' domain:', $job->domain, ', Incomplete software: ', \var_export($data, true));
            $result->software_name = 'unknow';
            throw new ParserException('Valid JSON but not Nodeinfo20');
        }

    }

    public static function nextParser(Job $job, Result $result): ?string
    {
        switch ($result->software_name) {
            case 'mastodon':
                return MastodonPeers::class;
            case 'peertube':
                return PeertubeFollowers::class;
            case 'pleroma':
                return PleromaMetadata::class;
            default:
        }
        return null;
    }

    public static function getUrl(Job $job): string
    {
        return 'https://' . $job->domain . self::URI;
    }
}

<?php

declare(strict_types=1);

namespace Cyrille37\MastoInstNet\Crawler\Parsers;

class ParserException extends \Exception
{
    public function __construct()
    {
        parent::__construct('Parser error',0);

    }
}
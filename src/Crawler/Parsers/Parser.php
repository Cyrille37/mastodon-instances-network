<?php

declare(strict_types=1);

namespace Cyrille37\MastoInstNet\Crawler\Parsers;

use Cyrille37\MastoInstNet\Crawler\Job;
use Cyrille37\MastoInstNet\Crawler\Result;

abstract class Parser
{
    const NODEINFO_20 = 'ni20';
    const MASTODON_PEERS = 'mt.p';
    const PEERTUBE_FOLLOWERS = 'pt.f';
    const MISSKEY_PEERS = 'ms.p';

    public abstract static function parse($content, Job $job, Result $result): void ;
    public abstract static function nextParser(Job $job, Result $result): ?string ;
    public abstract static function getUrl(Job $job): string;

}

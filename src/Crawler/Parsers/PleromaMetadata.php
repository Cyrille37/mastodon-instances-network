<?php

declare(strict_types=1);

namespace Cyrille37\MastoInstNet\Crawler\Parsers;

use Cyrille37\MastoInstNet\Crawler\Job;
use Cyrille37\MastoInstNet\Crawler\Result;
use Cyrille37\MastoInstNet\Out;

/**
 * Call again '/nodeinfo/2.0.json' to look in "metadata" for "mastodon_api" feature,
 * if "mastodon_api" presents it will ask for MastodonPeers parser to retreive subscribers.
 * 
 * https://docs-develop.pleroma.social/backend/development/API/nodeinfo/
 */
class PleromaMetadata extends Parser
{
    const URI = '/nodeinfo/2.0.json';

    public static function parse($content, Job $job, Result $result): void
    {
        //Out::println(__METHOD__,' content: ', $content);

        $data = json_decode($content);
        if (!$data)
            throw new ParserException();

        if( isset($data->metadata->features) && in_array('mastodon_api',$data->metadata->features) )
        {
            $job->parserData['has_mastodon_api'] = true ;
        }
    }

    public static function nextParser(Job $job, Result $result): ?string
    {
        if( is_array($job->parserData) && isset($job->parserData['has_mastodon_api']) )
        {
            return MastodonPeers::class;
        }
        return null;
    }

    public static function getUrl(Job $job): string
    {
        $job->parserData = null ;
        return 'https://' . $job->domain . self::URI;
    }

}

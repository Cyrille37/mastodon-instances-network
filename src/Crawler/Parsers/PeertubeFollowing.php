<?php

declare(strict_types=1);

namespace Cyrille37\MastoInstNet\Crawler\Parsers;

use Cyrille37\MastoInstNet\Crawler\Job;
use Cyrille37\MastoInstNet\Crawler\Result;
use Cyrille37\MastoInstNet\Out;

/**
 * https://docs.joinpeertube.org/api-rest-reference.html#tag/Instance-Follows/paths/~1api~1v1~1server~1following/get
 */
class PeertubeFollowing extends Parser
{
    const URI = '/api/v1/server/following';
    const ITEMS_PER_PAGE = 100;

    public static function parse($content, Job $job, Result $result): void
    {
        $data = json_decode($content);
        if (!$data)
            throw new ParserException();

        $paginate = [
            'total' => $data->total,
            'count' => 0,
        ];

        if (is_array($job->parserData) && isset($job->parserData['count']))
            $paginate['count'] = $job->parserData['count'];

        if (!is_array($result->subscriptions))
            $result->subscriptions = [];

        foreach ($data->data as $datum) {
            $paginate['count']++;
            //if ($datum->state != 'accepted')
            //    continue;
            $result->subscriptions[] = $datum->following->host;
        }
        $job->parserData = &$paginate;
    }

    public static function getUrl(Job $job): string
    {
        $start = 0;
        if (is_array($job->parserData) && isset($job->parserData['count']))
            $start = $job->parserData['count'];

        return 'https://' . $job->domain . self::URI
            // "id" and "host" field does not work :(
            //. '?sort=id'
            . '?'
            . '&count=' . self::ITEMS_PER_PAGE
            . '&state=accepted'
            . '&start=' . $start;
    }

    public static function nextParser(Job $job, Result $result): ?string
    {
        if ($job->parserData['count'] < $job->parserData['total'])
            return self::class;
        $job->parserData = null ;
        return null;
    }

}

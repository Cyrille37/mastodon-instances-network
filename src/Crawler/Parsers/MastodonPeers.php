<?php

declare(strict_types=1);

namespace Cyrille37\MastoInstNet\Crawler\Parsers;

use Cyrille37\MastoInstNet\Crawler\Job;
use Cyrille37\MastoInstNet\Crawler\Result;
use Cyrille37\MastoInstNet\Out;

/**
 * https://docs.joinmastodon.org/methods/instance/
 */
class MastodonPeers extends Parser
{
    const URI = '/api/v1/instance/peers';

    public static function parse($content, Job $job, Result $result): void
    {
        $data = json_decode($content);
        if (!$data)
            throw new ParserException();

        if (!is_array($result->subscribers))
            $result->subscribers = [];

        foreach ($data as $datum) {
            $result->subscribers[] = $datum;
        }
    }

    public static function getUrl(Job $job): string
    {
        return 'https://' . $job->domain . self::URI;
    }

    public static function nextParser(Job $job, Result $result): ?string
    {
        $job->parserData = null ;
        return null;
    }
}

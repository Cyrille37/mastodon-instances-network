<?php

declare(strict_types=1);
// tick use required for pcntl_signal
declare(ticks=1);

namespace Cyrille37\MastoInstNet\Crawler;

use Cyrille37\MastoInstNet\Common;
use Cyrille37\MastoInstNet\Crawler\Parsers\MastodonPeers;
use Cyrille37\MastoInstNet\Crawler\Parsers\MisskeyPeers;
use Cyrille37\MastoInstNet\Crawler\Parsers\PeertubeFollowers;
use Cyrille37\MastoInstNet\Crawler\Parsers\Nodeinfo20;
use Cyrille37\MastoInstNet\Crawler\Parsers\ParserException;
use Cyrille37\MastoInstNet\Env;
use Cyrille37\MastoInstNet\Out;
use Cyrille37\MastoInstNet\Stats;
use GuzzleHttp\Client as HttpClient;
use GuzzleHttp\Psr7\Response;
use GuzzleHttp\Exception\TransferException;
use GuzzleHttp\Pool;

class Crawler
{
    public static $DEBUG_HTTP = false;

    /**
     * @var HttpClient
     */
    protected $httpClient;

    protected $batch;
    protected $results;
    protected $batchsIndex;

    public static function getConcurrency()
    {
        return Common::getCrawlerConcurrency();
    }

    public function crawl(HttpClient $httpClient, array $domains): array
    {
        $this->httpClient = $httpClient;

        /*
        $batch[
            // $batchsIndex
            [
                // Jobs
            ]
        ]
        */
        $this->batch = [];
        $this->batch[] = [];
        $this->results = [];

        foreach ($domains as $domain) {
            $job = new Job($domain);
            $this->batch[0][] = $job;
            $this->results[$job->domain] = new Result();
        }

        $this->batchsIndex = 0;
        while (isset($this->batch[$this->batchsIndex]) && count($this->batch[$this->batchsIndex]) > 0) {

            Out::debug('Pooling batchsIndex:', $this->batchsIndex, ' batchs:', count($this->batch[$this->batchsIndex]), ', concurrency:', static::getConcurrency());
            $pool = new Pool($httpClient, $this->requestsGenerator($this->batch[$this->batchsIndex]), [
                'concurrency' => static::getConcurrency(),
                'fulfilled' => [$this, 'onSuccess'],
                'rejected' => [$this, 'onError'],
            ]);
            // Initiate the transfers and create a promsise
            $promise = $pool->promise();
            // Force the pool of requests to complete.
            $promise->wait();
            $this->batchsIndex++;
        };

        return $this->results;
    }

    protected function requestsGenerator($batch)
    {
        if (!Common::isInternetAvailable()) {
            die('ERROR: Internet unavailable.' . "\n");
        }
        foreach ($batch as $job) {
            yield function () use ($job) {
                if (!isset($job->parser))
                    // Set to the first (default) parser
                    $job->parser = Nodeinfo20::class;
                $url = $job->parser::getUrl($job);
                if (static::$DEBUG_HTTP)
                    Out::debug('initiate request: ', $url, ' for ', $job->domain);
                Stats::inc('Crawler.requests');
                return $this->httpClient->getAsync($url);
            };
        }
    }

    public function onError(TransferException $reason, $index)
    {
        Stats::inc('Crawler.error');
        /**
         * @var Job $job
         */
        $job = $this->batch[$this->batchsIndex][$index];
        if (static::$DEBUG_HTTP)
            Out::error('FAILED ', $job->domain, ' #', $index, '/', $this->batchsIndex, ', status:', $reason->getCode(), ', error: ', $reason->getMessage());

        $res = $this->results[$job->domain];

        $res->error_parser = $job->parser;

        switch (get_class($reason)) {
            case \GuzzleHttp\Exception\ConnectException::class:
            case \GuzzleHttp\Exception\RequestException::class:
                $res->error_code = 'error.curl';
                preg_match('/cURL error (\d+)\:/', $reason->getMessage(), $m);
                //$res->error_reason = '('.$m[1].') '. $reason->getMessage();
                if( ! isset($m[1]))
                    $res->error_reason = $reason->getMessage();
                else
                    $res->error_reason = $m[1];
                break;
            case \GuzzleHttp\Exception\TooManyRedirectsException::class:
            case \GuzzleHttp\Exception\ClientException::class:
            case \GuzzleHttp\Exception\ServerException::class:
                $res->error_code = 'error.http';
                $res->error_reason = $reason->getCode();
                //$res->error_reason = get_class($reason).': '.$reason->getMessage();
                break;
            default:
                $res->error_code = 'error';
                $res->error_reason = get_class($reason) . ': (' . $reason->getCode() . ') ' . $reason->getMessage();
        }
    }

    public function onSuccess(Response $response, $index)
    {
        Stats::inc('Crawler.success');
        /**
         * @var Job $job
         */
        $job =  $this->batch[$this->batchsIndex][$index];
        if (static::$DEBUG_HTTP)
            Out::debug('SUCCESS ', $job->domain, ' #', $index, '/', $this->batchsIndex, ', status:', $response->getStatusCode());

        // Init next batch
        if (!isset($this->batch[$this->batchsIndex + 1]))
            $this->batch[$this->batchsIndex + 1] = [];

        $content = $response->getBody()->getContents();
        try {
            $job->parser::parse($content, $job, $this->results[$job->domain]);
            //Out::println(__METHOD__,' $results: ',var_export($results,true));
            $nextParser = $job->parser::nextParser($job, $this->results[$job->domain]);
            if ($nextParser) {
                $job->parser = $nextParser;
                $this->batch[$this->batchsIndex + 1][] = $job;
            }
        } catch (\Exception $ex) {
            //Out::error( $content );
            //Out::error('ParserException: domain:', $job->domain,', url:',$job->parser::getUrl($job));
            //throw $ex;
            $res = $this->results[$job->domain];
            $res->error_code = 'error.parser';
            $res->error_reason = get_class($ex) . ': ' . $ex->getMessage();
            $res->error_parser = $job->parser;
        }
    }
}

<?php

declare(strict_types=1);

namespace Cyrille37\MastoInstNet\Crawler;

class Result {
    public $error_code ;
    public $error_reason ;
    public $error_parser ;

    /**
     * @var string
     */
    public $software_name ;
    /**
     * @var string
     */
    public $software_version ;
    /**
     * @var bool
     */
    public $openRegistrations ;
    /**
     * @var int
     */
    public $localPosts ;
    /**
     * @var int
     */
    public $localComments ;
    /**
     * @var int
     */
    public $users_total ;
    /**
     * @var int
     */
    public $users_activeMonth ;
    /**
     * @var int
     */
    public $users_activeHalfyear ;


    /**
     * @var array
     */
    public $subscribers ;
    /**
     * @var array
     */
    public $subscriptions ;

    public function isError()
    {
        return $this->error_code ? true : ($this->error_reason ? true : false ) ;
    }
}
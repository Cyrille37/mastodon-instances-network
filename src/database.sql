
CREATE TABLE "servers" (
	"id"	INTEGER PRIMARY KEY AUTOINCREMENT ,
	"domain"	TEXT NOT NULL,
	"type"		TEXT,
	"created_at"	TEXT NOT NULL
);
create unique index server_domains on servers(domain) ;

CREATE TABLE "servers_status" (
	"server_id"	INTEGER NOT NULL,
	"status"	TEXT NOT NULL,
	"value"		TEXT NOT NULL,
	"created_at"	TEXT NOT NULL,
    FOREIGN KEY(server_id) REFERENCES servers(id)
);
-- create index servers_status_server_id on servers_status(server_id) ;

CREATE TABLE "servers_peers" (
	"server_from"	INTEGER NOT NULL,
	"server_to"	INTEGER NOT NULL,
	"created_at"	TEXT NOT NULL,
	"updated_at"	TEXT NOT NULL,
    FOREIGN KEY(server_from) REFERENCES servers(id),
    FOREIGN KEY(server_to) REFERENCES servers(id)
);
CREATE INDEX "server_peers_from" ON "servers_peers" (
	"server_from"	ASC
);
-- create index servers_peers_from on servers_status(server_id) ;
-- create index servers_peers_to on servers_status(server_id) ;

<?php

declare(strict_types=1);

namespace Cyrille37\MastoInstNet;

/**
 * 
 * @property integer $server_id
 * @property string $status
 * @property string $value
 * @property string $created_at
 */
class ServersStatus extends DAO
{
	public static $table = 'servers_status';
	public static $created_at = true;

	static public function hasStatusSince($server, \DateTimeImmutable $date)
	{
		$sql = 'select 1 from ' . static::$table . ' where server_id=:server_id and created_at > :date';
		$sth = self::$db->prepare($sql);
		$sth->execute(['server_id' => $server->id, 'date' => $date->format(DAO::DATE_FORMAT)]);
		return $sth->fetch(\PDO::FETCH_ASSOC) ? true : false;
	}

	static public function lastError($server)
	{
		$sql = 'select * from ' . static::$table . ' where server_id=:server_id'
			.' and status like "error.%"'
			.' order by created_at desc limit 1';
		$sth = self::$db->prepare($sql);
		$sth->execute(['server_id' => $server->id]);
		return $sth->fetch(\PDO::FETCH_OBJ);
	}

	static public function setStatus($server, $key, $value)
	{
		self::insert(['server_id' => $server->id, 'status' => $key, 'value' => $value]);
	}

	static public function setStatusMany($server, array $statuses)
	{
		$createArgs = [];
		foreach( $statuses as $k => $v)
		{
			$createArgs[] = ['status'=>$k,'value'=>$v];
		}
		self::insertMany(['server_id' => $server->id], $createArgs);
	}

	static public function isError($status)
	{
		if( $status != null && substr($status->status,0,5) == 'error' )
			return true ;
		return false ;
	}
}

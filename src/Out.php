<?php

declare(strict_types=1);

namespace Cyrille37\MastoInstNet;

/**
 * 
 */
class Out
{
    protected $enabled;
    static protected $instance;

    protected function __construct()
    {
        $this->enable = Env::get('OUTPUT_ENABLE');
    }

    static public function getInstance()
    {
        if (self::$instance)
            return self::$instance;
        self::$instance = new static();
        return self::$instance;
    }

    static public function println(...$items): void
    {
        if (!static::getInstance()->enable)
            return;
        $items[] = "\n";
        self::print(...$items);
    }

    static public function print(...$items): void
    {
        if (!static::getInstance()->enable)
            return;
        foreach ($items as $item) {
            echo $item;
        }
    }

    static public function debug(...$items): void
    {
        if( DEBUG )
            self::println('DEBUG: ', ...$items);
    }

    static public function error(...$items): void
    {
        echo 'ERROR: ';
        foreach ($items as $item) {
            echo $item;
        }
        echo "\n";
    }
}

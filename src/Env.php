<?php

declare(strict_types=1);

namespace Cyrille37\MastoInstNet;

use DevCoder\DotEnv;

/**
 * 
 */
class Env
{
    protected static $instance ;

    protected static function getInstance()
    {
        if( ! self::$instance )
        {
            $envFile = __DIR__ . '/../.env';
            if (file_exists($envFile))
            (new DotEnv($envFile))->load();    
            self::$instance = new static();
        }
        return self::$instance ;
    }

    static public function get($k, $default = null)
    {
        self::getInstance();
        $r = \getenv($k);
        if ($r === false)
            return $default;
        return $r;
    }
}

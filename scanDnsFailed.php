#!/usr/bin/env php
<?php
/**
 * ./scanDnsFailed.php
 * 
 * Scan les serveurs marqué "DNS Failed"
 */

declare(strict_types=1);
// tick use required for pcntl_signal
declare(ticks=1);

require_once('vendor/autoload.php');

use Cyrille37\MastoInstNet\Common;
use Cyrille37\MastoInstNet\Out;
use Cyrille37\MastoInstNet\Scanner;
use Cyrille37\MastoInstNet\Servers;
use Cyrille37\MastoInstNet\ServersStatus;
use Cyrille37\MastoInstNet\Stats;

Common::init();

/**
 * @var Servers $server
 */
foreach (Servers::all() as $server) {

    $status = ServersStatus::lastError($server);

    if ($status && ServersStatus::isError($status)) {

        //Out::println($server->domain, ': DNS OK: ', $dns[0]['ip'], ', status: ', $status->status, ': ', $status->value);
        switch ($status->status) {
            case 'error.curl':
                switch ($status->value) {
                    case '6': // CURLE_COULDNT_RESOLVE_HOST

                        Stats::inc('Scan.DnsFailed.servers');

                        // https://www.php.net/manual/fr/function.dns-get-record
                        $dns = dns_get_record($server->domain, DNS_A);

                        if( $dns )
                        {
                            Out::println($server->domain, ': DNS OK but previous SCAN FAILED to resolve');
                            Scanner::scan_server($server->domain, 1);    
                        }
                        break;
                }
                break;
        }
    }
}

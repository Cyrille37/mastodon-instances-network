#!/usr/bin/env php
<?php
/**
 * ./scan.php
 * options:
 *  - depth
 * arguments:
 *  - domain name
 */

declare(strict_types=1);
// tick use required for pcntl_signal
declare(ticks=1);

require_once('vendor/autoload.php');

use Cyrille37\MastoInstNet\Common;
use Cyrille37\MastoInstNet\Crawler\Crawler;
use Cyrille37\MastoInstNet\Env;
use Cyrille37\MastoInstNet\Out;
use Cyrille37\MastoInstNet\Scanner;
use Cyrille37\MastoInstNet\Stats;

// Global options

$domain = $depth_max = null;
options_read();

Common::init();

$start_at = new \DateTimeImmutable('now', Common::getTimezone());


Stats::set('Config.Start at', $start_at->format('Y-m-d H:i:s'));

pcntl_signal(2, 'pcntl_signal_handler');
pcntl_signal(20, 'pcntl_signal_handler');
/**
 * Don't forgat "declare(ticks = 1);"
 */
function pcntl_signal_handler($signal)
{
    Out::println('Stats:');
    Out::println(\var_export(Stats::stats(), true));
    die('Aborted with Ctrl+C' . "\n");
}

Out::println('Stats: ', \var_export(Stats::stats(), true));

Scanner::scan_server($domain, $depth_max);

Out::println('Stats: ', \var_export(Stats::stats(), true));

function options_read()
{
    global $argv, $argc, $depth_max, $domain;

    try {
        // '': no value, ':' value mandatory, '::' optional value
        $long_options = ['depth::'];
        $rest_index = null;
        $options = getopt('', $long_options, $rest_index);

        if (isset($options['depth']))
            $depth_max = intval($options['depth']);
        else
            $depth_max = Env::get('SCAN_DEPTH', 10);
        if ($depth_max < 0)
            throw new \InvalidArgumentException('Depth must be a positive integer.');

        $args = array_slice($argv, $rest_index);
        if (count($args) != 1)
            throw new InvalidArgumentException('Invalid arguments.');

        $domain = $args[0];
        if (!$domain)
            throw new InvalidArgumentException('A domain name please ;-)');

        //Out::error(var_export($options,true));
        //Out::error(var_export($args,true));

    } catch (\InvalidArgumentException $ex) {
        Out::error($ex->getMessage());
        Out::println('Usage: scan --depth=N <domain name>');
        die();
    }
}
